import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { GoogleMapsModule } from '@angular/google-maps';

import { RootComponent } from './containers/root/root.component';

@NgModule({
  declarations: [RootComponent],
  imports: [BrowserModule, GoogleMapsModule],
  providers: [],
  bootstrap: [RootComponent]
})
export class AppModule {}
