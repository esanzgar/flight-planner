import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fp-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.scss']
})
export class RootComponent implements OnInit {
  path: google.maps.LatLng[] = [];
  zoom = 20;
  center: google.maps.LatLngLiteral;
  options: google.maps.MapOptions = {
    zoomControl: false,
    scrollwheel: false,
    disableDoubleClickZoom: true,
    maxZoom: 20,
    minZoom: 16
  };

  ngOnInit() {
    navigator.geolocation.getCurrentPosition(position => {
      this.center = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
    });
  }

  click(event: google.maps.MouseEvent) {
    this.path.push(event.latLng);
  }
}
