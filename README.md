# FlightPlanner

This Angular app (version 9.0.0-rc.8) is a flight planner for drones.

## Development server

To develop locally run the development server following one of these recipes:

```
yarn install [--frozen-lockfile]
yarn start
```

## Log

- 3pm: start initial setup
- 3:30pm: finish initial CI/CD
- 4-4:30pm: doing other business
- 4:45pm: finish initial setup of google maps
- 4:45pm-5:30pm: learn to add markers so I can move to polylines
- 5:30-6pm: realised that polylines are not yet included in the release
  candidate of @angular/google-maps. HORROR!

## Want to help?

Want to file a bug, contribute some code, or improve documentation? Excellent!
Read up on our guidelines for [contributing][contributing].

[contributing]: https://gitlab.com/esanzgar/flight-planner/blob/master/CONTRIBUTING.md
